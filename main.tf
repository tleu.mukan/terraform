
terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "=3.0.0"
    }
  }

}
provider "azurerm" {
    features {}
}

resource "azurerm_resource_group" "InsTechRG" {
  name                     = "InsTechRG"
  location                 = "northeurope"
}

resource "azurerm_storage_account" "InsTechRGstorage" {
  name                     = "tmwukanteststorage"
  resource_group_name      = azurerm_resource_group.InsTechRG.name
  location                 = azurerm_resource_group.InsTechRG.location
  account_tier             = "Standard"
  account_replication_type = "GRS"

  tags = {
    environment = "staging"
  }
}
// data "azurerm_resource_group" "InsTechRG" {
//   name     = "InsTechRG"
// }

// output "example" {
//   value                 = data.azurerm_resource_group.example 
// }
// resource "azurerm_storage_account" "example" {
//   name                     = "tmexamsa001"
//   resource_group_name      = azurerm_resource_group.example.name
//   location                 = azurerm_resource_group.example.location
//   account_tier             = "Standard"
//   account_replication_type = "GRS"

//   tags = {
//     environment = "staging"
//   }
// }
